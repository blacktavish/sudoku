package CSV;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Component
public class CSVReaderImpl implements CSVReader{

    private List<String> line;

    public CSVReaderImpl(List<String> line){
        this.line = line;
    }

    @Override
    public List<String> readFile(String fileName,String divider){
        try{
            BufferedReader reader = new BufferedReader(new FileReader(PATH+ fileName));
            String lines;
            reader.readLine();
            while ((lines = reader.readLine()) != null ) {line.add(lines);}
        } catch (IOException e) {e.printStackTrace(); }
        return line;
    }
}
